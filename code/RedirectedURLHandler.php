<?php
/**
 * Handles the redirection of any url from a controller. Apply this to your controller using
 *
 * <code>
 * Controller::add_extension("Controller", "RedirectedURLHandler");
 * </code>
 *
 * @package redirectedurls
 * @author sam@silverstripe.com
 * @author scienceninjas@silverstripe.com
 */
class RedirectedURLHandler extends Extension {

	/**
	 * Converts an array of key value pairs to lowercase
	 *
	 * @param array $vars key value pairs
	 * @return array
	 */
	protected function arrayToLowercase($vars) {
		$result = array();

		foreach($vars as $k => $v) {
			if(is_array($v)) {
				$result[strtolower($k)] = $this->arrayToLowercase(strtolower($v));
			} else {
			    $result[strtolower($k)] = strtolower($v);
            }
		}

		return $result;
	}

    /**
     * @throws SS_HTTPResponse_Exception
     */
    public function onBeforeHTTPError404($request) {
        if (!Director::isLive()) {
            return;
        }

        $base    = $_SERVER['REQUEST_URI']; //strtolower($request->getURL());
        $getVars = $this->arrayToLowercase($request->getVars());
        unset($getVars['url']);

        $subdomainConfigClass = Config::inst()->get('RedirectedURLHandler', 'subdomain_class');
        $subdomainConfigProperty = Config::inst()->get('RedirectedURLHandler', 'subdomain_property');

        $subdomains = array();
        if ($subdomainConfigClass && $subdomainConfigProperty) {
            $subdomains = Config::inst()->get($subdomainConfigClass, $subdomainConfigProperty);
        }

        $host      = $request->getHeader('Host');
        $subDomain = $this->getSubDomain($host);

        // we always want to redirect to main domain if subdomain was found
        $domain = null;
        if (!empty($subDomain)) {
            $domain = $this->getDomain($host);
        }

        $subDomainNotExisting = false;
        if (count($subdomains) && array_search($subDomain, $subdomains) === false) {
            $subDomainNotExisting = true;
        }

        // Find all the RedirectedURL objects where the base URL matches.
        // Assumes the base url has no trailing slash.
        $SQL_base = Convert::raw2sql(rtrim($base, '/'));

        $filter = str_replace('//','/', "\"FromBase\" = '" . $SQL_base . "'");
        $potentials     = DataObject::get("RedirectedURL",
            $filter,
            "\"FromQuerystring\" ASC");

        $listPotentials = new ArrayList;
        foreach ($potentials as $potential) {
            $listPotentials->push($potential);
        }

        // Find any matching FromBase elements terminating in a wildcard /*
        $baseparts = explode('/', $base);
        for ($pos = count($baseparts) - 1; $pos >= 0; $pos --) {
            $basestr  = implode('/', array_slice($baseparts, 0, $pos));
            $basepart = Convert::raw2sql($basestr . '/*');
            $basepots = DataObject::get("RedirectedURL",
                "\"FromBase\" = '/" . $basepart . "'",
                "\"FromQuerystring\" ASC");
            foreach ($basepots as $basepot) {
                // If the To URL ends in a wildcard /*, append the remaining request URL elements
                if (substr($basepot->To, - 2) === '/*') {
                    $basepot->To = substr($basepot->To, 0, - 2) . substr($base, strlen($basestr));
                }
                $listPotentials->push($basepot);
            }
        }

        $matched = null;

        // Then check the get vars, ignoring any additional get vars that
        // this URL may have
        if ($listPotentials) {
            foreach ($listPotentials as $potential) {
                $allVarsMatch = true;

                if ($potential->FromQuerystring) {
                    $reqVars = array();
                    parse_str($potential->FromQuerystring, $reqVars);

                    foreach ($reqVars as $k => $v) {
                        if ( ! $v) {
                            continue;
                        }

                        if ( ! isset($getVars[ $k ]) || $v != $getVars[ $k ]) {
                            $allVarsMatch = false;
                            break;
                        }
                    }
                }

                if ($allVarsMatch) {
                    $matched = $potential;
                    break;
                }
            }
        }

        // logic:
        // path is either the found redirect target or the original request path
        // we found a subdomain that's known - we redirect to domain/subdomain/path
        // we found a subdomain that's not known - we redirect to domain/path
        // we didn't find a subdomain but found a redirect target path - we redirect to target path
        if ($matched || !empty($subDomain)) {
            if ($matched) {
                $matched = $matched->To;
            } else {
                $matched = $base;
            }

            $response = new SS_HTTPResponse();
            if ($subDomainNotExisting) {
                if (defined('forceWWW') && forceWWW) {
                    $subDomain = 'www';
                } else {
                    $subDomain = '';
                }

            }
            $dest     = Controller::join_links($subDomain, $matched);

            if ($domain) {
                $protocol = Director::protocol();
                $response->redirect(Controller::join_links($protocol . $domain, $dest) , 301);

            } else {
                $response->redirect(Director::absoluteURL($dest), 301);
            }

            throw new SS_HTTPResponse_Exception($response);
        }
    }

    protected function getSubDomain($hostString) {
        $parsedUrl = parse_url('//' . $hostString);
        $host      = explode('.', $parsedUrl['host']);

        $stop = $this->getDomainSplit($hostString);

        $rest = array_splice($host, 0, -$stop);
        array_pop($rest);

        return implode('.', $rest);
    }

    protected function getDomain($hostString) {
        $parsedUrl = parse_url('//' . $hostString);
        $host      = explode('.', $parsedUrl['host']);

        $stop = $this->getDomainSplit($hostString);

        return implode('.', array_splice($host, -($stop + 1)));
    }

    protected function getDomainSplit($host) {
        $parsedUrl = parse_url('//' . $host);
        $host      = explode('.', $parsedUrl['host']);

        // go through backwards, we assume as long as length of part is 3 or less, it's part of TLD
        $stop = false;
        $i=0;
        foreach (array_reverse($host) as $part) {
            if (strlen($part) > 3) {
                $stop = $i;
                break;
            }
            $i++;
        }
        return $stop;
    }

    public function onBeforeInit() {
        $request = $this->owner->getRequest();

        $host      = $request->getHeader('Host');
        $subDomain = $this->getSubDomain($host);

        if (!empty($subDomain)) {
            $this->onBeforeHTTPError404($request);
        }
    }
}
