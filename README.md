Subdomain Redirect URLs
===============

this is a fork from https://github.com/silverstripe/silverstripe-redirectedurls

internal use mostly, since it does a specific redirect by sudomain:

subdomain.domain.tld/path => domain.tld/subdomain/path
